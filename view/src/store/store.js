import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        results: [],
        suggestions: [],
        numMatches: 0,
        field: "description"
    },

    mutations: {
        
        setSearchResults: (state, { results }) => {
            state.results = results;
        },

        setNumMatches: (state, { numMatches }) => {
            state.numMatches = numMatches;
        },

        setField: (state, { field }) => {
            state.field = field;
        },

        setSuggestions: (state, { suggestions }) => {
            state.suggestions = suggestions;
        }

    },

    actions: {
        getSearchResults: (context, url) => {
            axios
                .get(url)
                .then(response => {
                    context.commit('setSearchResults', { results: response.data.results })
                    context.commit('setNumMatches', { numMatches: response.data.numMatches })
                    console.log(response.data)
                })
                .catch(error => {
                    console.log(error);
                })
        },

        setSelectedField: (context, field) => {
            context.commit('setField', { field: field })
        },

        updateSuggestions: (context, url) => {
            if (context.state.field !== "name")
                return;
            axios
                .get(url)
                .then(response => {
                    context.commit('setSuggestions', { suggestions: response.data.suggestions })
                })
                .catch(error => {
                    console.log(error)
                })
        }
    }
})