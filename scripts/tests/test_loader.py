import unittest

from rc import loader

class TestLoader(unittest.TestCase):

    def test_LoadingEmptyStringRaisesException(self):
        with self.assertRaises(Exception) as context:
            class_loader = loader.Loader("")

    def test_LoadingClassWorks(self):
        class_loader = loader.Loader("tests/resources", "test_module", "TestClass")
        test_class = class_loader.get_class()
        test_instance = test_class("testing")
        self.assertEqual("testing", test_instance.get_str())

if __name__ == '__main__':
    unittest.main()

