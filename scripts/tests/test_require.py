import unittest

from rc import require

class TestRequirements(unittest.TestCase):

    def test_CanCreateRequirementsInstance(self):
        reqs = require.Requirements()

    def test_CanAddRequiredFile(self):
        reqs = require.Requirements()
        reqs.add_required_file("test")
        self.assertEqual("test", reqs.get_required_files()[0])

    def test_CanAddMultipleRequiredFiles(self):
        reqs = require.Requirements()
        reqs.add_required_file("file_1")
        reqs.add_required_file("file_2")
        reqs.add_required_file("file_3")
        self.assertEqual("file_1", reqs.get_required_files()[0])
        self.assertEqual("file_2", reqs.get_required_files()[1])
        self.assertEqual("file_3", reqs.get_required_files()[2])

    def test_CanAddRequiredDirectory(self):
        reqs = require.Requirements()
        reqs.add_required_directory("test")
        self.assertEqual("test", reqs.get_required_directories()[0])

    def test_EnforceRaisesIfRequiredFileNotFound(self):
        reqs = require.Requirements()
        reqs.add_required_file("test")
        with self.assertRaises(Exception) as context:
            reqs.enforce()

    def test_EnforceRaisesIfRequiredDirectoryNotFound(self):
        reqs = require.Requirements()
        reqs.add_required_directory("test")
        with self.assertRaises(Exception) as context:
            reqs.enforce()