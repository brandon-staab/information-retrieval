import os
import unittest
import shutil

from rc import parameters

class TestParameters(unittest.TestCase):

    def test_ConstructingParametersRaisesIfFileNotFound(self):
        with self.assertRaises(Exception) as context:
            parameters.Parameters("", "")

    def test_GetMapReturnsFlattenedListOfKeyValuePairs(self):
        p = parameters.Parameters("tests/resources", "parameters.yml")
        values = p.get_map()
        self.assertEqual(5, len(values))

    def test_ReplaceInFileSubstitutesValuesForAllVariables(self):
        with open("tests/resources/test.txt", "w") as stream:
            stream.write("%%variables.test_var_1%%")

        p = parameters.Parameters("tests/resources", "parameters_2.yml")
        p.add_target_file("test.txt")
        p.replace_variables()

        with open("tests/resources/test.txt", 'r') as stream:
            line_contents = stream.readline()
        os.remove("tests/resources/test.txt")
        self.assertEqual("test_value", line_contents)

    def test_ReplaceInDirectorySubstitutesValuesForAllVariablesRecursively(self):
        shutil.copytree("tests/resources", "temp")

        p = parameters.Parameters("temp", "parameters_3.yml")
        p.add_target_directory("replace_tests")
        p.replace_variables()

        with open("temp/replace_tests/fileOne.txt", "r") as stream:
            self.assertEqual("a", stream.readline())
        with open("temp/replace_tests/fileTwo.txt", "r") as stream:
            self.assertEqual("b", stream.readline())

        shutil.rmtree("temp")
