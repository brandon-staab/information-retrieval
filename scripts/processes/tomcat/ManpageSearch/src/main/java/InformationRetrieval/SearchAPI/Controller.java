package InformationRetrieval.SearchAPI;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("api")
public class Controller {

    @GET
    @Path("search")
    @Produces(MediaType.APPLICATION_JSON)
    public String getQueryResults (@QueryParam("field") String field, @QueryParam("keywords") String keywords) {
    	
    	// empty query receives empty response
    	if (keywords == null || keywords.isEmpty())
    		return "";
    	
    	try {
			SolrClient solrClient = new SolrClient();
			solrClient.sendQuery(field + ":" + keywords);
			return solrClient.getResultsAsJSON();
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    		return "error";
    	}
    	
    }

    @GET
    @Path("suggest")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSuggestions (@QueryParam("keywords") String keywords) {

        if (keywords == null || keywords.isEmpty())
            return "";

        try {
            SolrClient solrClient = new SolrClient();
            solrClient.sendSuggestionQuery(keywords);
            return solrClient.getSuggestionsAsJSON();
        }
        catch (Exception e) {
            e.printStackTrace();
            return "error";           
        }

    }
    
}
