package InformationRetrieval.SearchAPI;

import org.apache.solr.client.solrj.beans.Field;

public class Document {
	
	@Field public String id;
	@Field public String section;
	@Field public String page;
	@Field public String name;
	@Field public String synopsis;
	@Field public String description;
	@Field public String implementation;
	@Field public String see;
	@Field public String history;
	@Field public String authors;
	@Field public String examples;
	@Field public String copyright;
	@Field public String code;
	@Field public String functions;
	@Field public String locking;
	//@Field public String return;
	@Field public String compatibility;
	@Field public String how;
	@Field public String queuing;
	@Field public String errors;
	@Field public String version;

}
