package InformationRetrieval.SearchAPI;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

@Path("mock-api")
public class MockController {

	/**
	 * 
	 * @param query
	 * @return
	 */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getQueryResults (@QueryParam("query") String query) {
    	
    	// empty query receives empty response
    	if (query == null || query.isEmpty())
    		return "";
    	
    	JSONArray results = new JSONArray();
    	results.add(createResult("result 1", "Result of query."));
    	results.add(createResult("result 2", "Result of query."));
    	results.add(createResult("result 3", "Result of query."));
    	results.add(createResult("result 4", "Result of query."));
    	
    	JSONObject response = new JSONObject();
    	response.put("results", results);

    	return response.toJSONString();

    }
    
    private static Object createResult (String name, String description) {

    	JSONObject result = new JSONObject();
    	result.put("name", name);
    	result.put("description", description);
    	
    	return result;
    	
    }
}
