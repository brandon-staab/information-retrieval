package InformationRetrieval.SearchAPI;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class SolrClient {
	
	private static final String URL = "http://localhost:8983/solr";
	private static final String CORE_NAME = "man";
	private static final int PAGE_SIZE = 20;

	private List<Document> documents = new ArrayList<Document>();
	private List<Document> suggestions = new ArrayList<Document>();
	private int numMatches = 0;
	
	public void sendQuery (String queryText) throws SolrServerException, IOException {
		
		// create Solr client
		HttpSolrClient.Builder builder = new HttpSolrClient.Builder(URL);
		builder.withConnectionTimeout(5000);
		builder.withSocketTimeout(30000);
		HttpSolrClient solr = builder.build();
		
		// define query to be sent
		SolrQuery query = new SolrQuery(queryText);
		
		// paging
		query.setStart(0);
		query.setRows(PAGE_SIZE);
		
		// select fields
		query.addField("section");
		query.addField("page");
		query.addField("name");
		query.addField("synopsis");
		query.addField("description");
		query.addField("implementation");
		query.addField("history");
		query.addField("authors");
		query.addField("examples");
		query.addField("code");
		query.addField("functions");
		query.addField("errors");
		query.addField("version");

		// execute query
		QueryResponse response = solr.query(CORE_NAME, query);
		documents = response.getBeans(Document.class);
		numMatches = (int) response.getResults().getNumFound();
		
	}

	public String getResultsAsJSON () {

		// data structure to hold results
		JSONObject response = new JSONObject();

		// add number of results
		response.put("numMatches", numMatches);

		// add documents (in current page)
		JSONArray results = new JSONArray();
		for (Document document : documents) {
			JSONObject json = new JSONObject();
			json.put("section", document.section);
			json.put("page", document.page);
			json.put("name", document.name);
			json.put("synopsis", document.synopsis);
			json.put("description", document.description);
			json.put("implementation", document.implementation);
			json.put("history", document.history);
			json.put("authors", document.authors);
			json.put("examples", document.examples);
			json.put("code", document.code);
			json.put("functions", document.functions);
			json.put("errors", document.errors);
			json.put("version", document.version);
			results.add(json);
		}
		response.put("results", results);	

		return response.toJSONString();
	 
	}

	public void sendSuggestionQuery (String keywords) throws SolrServerException, IOException {

			// create Solr client
			HttpSolrClient.Builder builder = new HttpSolrClient.Builder(URL);
			builder.withConnectionTimeout(5000);
			builder.withSocketTimeout(30000);
			HttpSolrClient solr = builder.build();	

		  // define query to be sent
			SolrQuery query = new SolrQuery("name:" + keywords);
			query.setRequestHandler("suggest_name");

			// execute query
			QueryResponse response = solr.query(CORE_NAME, query);
			suggestions = response.getBeans(Document.class);

	}

	public String getSuggestionsAsJSON () {

		JSONObject response = new JSONObject();

		JSONArray suggestionArray = new JSONArray();
		for (Document suggestion : suggestions) {
			suggestionArray.add(suggestion.name);
		}
		response.put("suggestions", suggestionArray);

		return response.toJSONString();

	}

}
