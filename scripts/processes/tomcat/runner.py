import os
import subprocess

import parameters
import require

CONFIG_ROOT = os.path.dirname(os.path.realpath(__file__))

class Runner:

    def __init__(self):
        req = require.Requirements(CONFIG_ROOT)
        req.add_required_file("parameters.yml")
        req.add_required_file("docker-compose.yml")
        req.add_required_file("Dockerfile")
        req.enforce()
        print("All requirements satisfied.")

        params = parameters.Parameters(CONFIG_ROOT, "parameters.yml")
        params.add_target_file("docker-compose.yml")
        params.add_target_file("Dockerfile")
        params.replace_variables()
        print("Variables replaced")

    def run(self):
        # build the web resource
        subprocess.call(['mvn', '-f', CONFIG_ROOT + '/ManpageSearch/pom.xml', 'clean'])
        subprocess.call(['mvn', '-f', CONFIG_ROOT + '/ManpageSearch/pom.xml', 'package'])

        # spin up the container
        subprocess.call(['docker-compose', '-f', CONFIG_ROOT + '/docker-compose.yml', 'build'])
        subprocess.call(['docker-compose', '-f', CONFIG_ROOT + '/docker-compose.yml', 'up'])
