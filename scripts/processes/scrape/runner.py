import os
import subprocess

import parameters
import require

CONFIG_ROOT = os.path.dirname(os.path.realpath(__file__))

class Runner:

    def __init__(self):
        req = require.Requirements(CONFIG_ROOT)
        req.add_required_file("parameters.yml")
        req.add_required_file("main.py")
        req.add_required_file("process_manual.py")
        req.add_required_file("web.py")
        req.enforce()
        print("All requirements satisfied.")

        params = parameters.Parameters(CONFIG_ROOT, "parameters.yml")
        params.add_target_file("main.py")
        params.add_target_file("web.py")
        params.replace_variables()
        print("Variables replaced")

    def run(self):
        subprocess.call(['python3', CONFIG_ROOT + '/main.py'])
