from bs4 import BeautifulSoup as Soup
from process_manual import process_manual

import web


commit_size = %%solr.core.commit_size%%


def select_anchors(raw_html, alt):
	html = Soup(raw_html, 'html.parser')
	return [a['href'] for a in html.select(f'img[alt="[{alt}]"] + a')]


def get_sections():
	return select_anchors(web.get_page(''), 'DIR')


def get_pages(section):
	return select_anchors(web.get_page(section), 'TXT')


def process_manuals(section, pages):
	docs = []
	for page in pages:
		manual = section + page
		print(f'{process_manuals.indexed : 5} : {len(docs) : 4} {manual}')

		try:
			raw_html = web.get_page(manual)
			docs += [process_manual(section[:-1], page, raw_html)]
			process_manuals.indexed += 1
		except RuntimeError as e:
			print(f'skipping {manual} because of {e}')

		if len(docs) >= commit_size:
			web.commit_docs(docs)

	web.commit_docs(docs)


def main():
	process_manuals.indexed = 0
	web.init()
	web.delete_docs()

	for section in reversed(get_sections()):
		print(f'processing {section}')
		pages = get_pages(section)
		process_manuals(section, pages)


if __name__ == '__main__':
	main()
