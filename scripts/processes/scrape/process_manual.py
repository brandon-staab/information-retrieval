from bs4 import BeautifulSoup as Soup

import re

## Helper Functions ############################################################

def clean(s):
	s = s.lower()
	if s.isalnum():
		return s

	f = lambda c: c if c.isalnum() else ''
	return ''.join(map(f, s))


def normalize(text):
	"""
	Replaces duplicate spaces with a single space and duplicate newlines with a
	single newline.  Then removes spaces and newlines from be beginning and end
	of the string.
	"""
	text = re.sub(' {2,}', ' ', text)
	text = re.sub('\n{2,}', '\n', text)
	text = re.sub('^( |\\n)+|( |\\n)+$', '', text)
	return text


def extract_text(html_tree):
	if not hasattr(html_tree, 'get_text()'):
		return ""
	text = html_tree.get_text()
	return normalize(text)

## Processing ##################################################################

def process_manual(section, page, raw_html):
	meta =  {'section': section, 'page': page}
	data = process_raw_html(raw_html)
	return {**meta, **data}


def process_raw_html(raw_html):
	html_tree = Soup(raw_html, 'html.parser')
	content_wrapper = html_tree.select_one('#tableWrapper')
	section_headers = content_wrapper.select('h4')
	return process_sections(section_headers)


def process_sections(section_headers):
	sections = {}
	for header in section_headers:
		section_title = clean(extract_text(header))
		section_body = process_section_body(header.next_sibling)
		if section_body:
			sections[section_title] = section_body
	return sections


def process_section_body(section_body):
	return extract_text(section_body)
