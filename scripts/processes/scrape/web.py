from cachecontrol.adapter import CacheControlAdapter
from cachecontrol.caches import FileCache
from cachecontrol.heuristics import ExpiresAfter
from requests import post, Session
from requests.exceptions import RequestException
from time import sleep

import json


manpage_url = '%%sources.manpages.url%%/%%sources.manpages.version%%/%%sources.manpages.lang%%/'
core_url = 'http://localhost:%%solr.network.port%%/solr/%%solr.core.name%%/update/json'
delay = %%sources.manpages.delay%%
timeout = %%sources.manpages.timeout%%
cached_days = %%cache.days%%
cache_location = '%%cache.location%%'


def is_good_response(resp):
	content_type = resp.headers['Content-Type'].lower()
	return resp.status_code == 200 and content_type is not None

## Solr ########################################################################

def solr_post(data):
	resp = post(core_url, data=data)
	if not is_good_response(resp):
		raise RuntimeError(resp.status_code, resp.reason)


def commit_docs(docs):
	if len(docs) <= 0:
		return
	print('committing...')
	data = json.dumps(docs)
	solr_post(data)
	docs.clear()


def delete_docs():
	print('deleting documents...')
	solr_post('{ "delete": {"query":"*:*"} }')

## Manpages ####################################################################

def init():
	adapter = CacheControlAdapter(FileCache(cache_location, forever=True), heuristic=ExpiresAfter(days=cached_days))
	cached_get.sess = Session()
	cached_get.controller = adapter.controller
	cached_get.sess.mount('https://', adapter)
	cached_get.last_was_cached = False


def get_page(page):
	if not cached_get.last_was_cached:
		sleep(delay)

	raw_html = cached_get(manpage_url + page)
	if raw_html:
		return raw_html

	print('sleeping...')
	sleep(timeout)
	return get_page(page)


def cached_get(url):
	try:
		resp = cached_get.sess.get(url, stream=True)
		cached_get.last_was_cached = cached_get.controller.cached_request(resp.request)

		if not cached_get.last_was_cached:
			print('*')

		if is_good_response(resp):
			return resp.content

		if resp.status_code == 500:
			raise RuntimeError(f'{resp.status_code} - {resp.reason}')

		return None
	except RequestException as e:
		print(f'Error during requests to {url} : {str(e)}')
		return None
