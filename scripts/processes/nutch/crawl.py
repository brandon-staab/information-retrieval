#!/usr/bin/python

import argparse
import subprocess

def run_crawl (config_dir):

    # TODO: get the name (for Docker image and container)
    name = 'my_crawler'

    # remove any existing image with same name
    subprocess.call(['docker', 'rmi', '--force', name])

    # build the Docker image for given configuration
    subprocess.call(['docker', 'build', '-t', name, config_dir])

    # instantiate and run the container
    subprocess.call(['docker', 'run', '--rm', '-t', '-i', '--name', name, name])

if __name__ == '__main__':

    # parse command-line arguments
    parser = argparse.ArgumentParser(description="Launch a Nutch crawl.")
    parser.add_argument('path_to_config')
    args = parser.parse_args()

    # TODO: validate the input

    # trigger the crawl
    run_crawl(args.path_to_config)
