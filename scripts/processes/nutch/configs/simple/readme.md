## Configuration Checklist

Complete these steps in order to configure the crawl process:

1. **Specify URL seeds.**
 
	Create a seed list file (or a set of seed list files) in the
	"urls" directory.  The seed list files shall have one URL
	per line.  They are copied into the Nutch installation inside of
	the Docker image, and they are used to bootstrap the crawl database.
	
2. **Edit "nutch-site.xml"**

	This file is used to override the default Nutch settings defined
	in the file "conf/nutch-default.xml" file. For basic crawls, the
	only thing we need to do is add a property.  Specifically, we must
	add an HTTP agent name.

3. **Edit "regex-urlfilter.txt"**

	This file filters URLs based on a set of regular expressions following
	the Java Regex specification [2].

4. **Edit "index-writers.xml" in order to point Nutch to desired Solr instance

  TODO: Fill this out

###### References

1. [Nutch Tutorial](https://cwiki.apache.org/confluence/display/nutch/NutchTutorial)

2. [RegexURLFilter](https://nutch.apache.org/apidocs/apidocs-1.9/org/apache/nutch/urlfilter/regex/RegexURLFilter.html)
