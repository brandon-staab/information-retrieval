#!/bin/bash

echo "=============================================================="
echo " Executing the Crawl Script"
echo "=============================================================="

NUTCH_HOME=/root/nutch_source/runtime/local

# trigger the Apache Nutch crawl
${NUTCH_HOME}/bin/crawl -i -s ${NUTCH_HOME}/urls ${NUTCH_HOME}/crawl 2
