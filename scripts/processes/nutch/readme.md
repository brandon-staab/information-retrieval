# Crawl Framework

### Desiderata

 * Very small top-level interface
 * Version-controlled configurations
 * Easy archiving of crawl results
 * Isolation of individual crawls

### Top-level Design

The following UML diagram expresses the various components of our
process for running Nutch crawls.  Not all components and processes
will be implemented with software.  Some will be implemented with
manual human interaction.  We simply use UML as a design notation.

**Note:** Some of the "methods" will be implemented using manpower.

![UML Diagram of Crawl Framework](doc/images/crawl-framework.png)

### Examples

```
python crawler.py configurations/test_1
```
In this example, a Python script is executed in order to trigger the crawl
process.  A crawl process involves spinning up a runtime environment for
the crawl (based on the given configuration).  "test_1" refers to a directory
storing all of the files necessary to instantiate a crawl process, including
the desired Nutch configuration, the Solr connection parameters,
and the Dockerfile.