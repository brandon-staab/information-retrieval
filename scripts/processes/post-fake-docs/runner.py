import os
import subprocess

import parameters
import require

CONFIG_ROOT = os.path.dirname(os.path.realpath(__file__))

class Runner:

    def __init__(self):
        req = require.Requirements(CONFIG_ROOT)
        req.add_required_file("parameters.yml")
        req.add_required_file("post_fake_documents.sh")
        req.enforce()
        print("All requirements satisfied.")

        params = parameters.Parameters(CONFIG_ROOT, "parameters.yml")
        params.add_target_file("post_fake_documents.sh")
        params.replace_variables()
        print("Variables replaced")

    def run(self):
        subprocess.call(['bash', CONFIG_ROOT + '/post_fake_documents.sh'])
