curl \
  -H Content-type:text/json \
  http://localhost:%%solr.network.port%%/solr/%%solr.core.name%%/update?commit=true \
  --data-binary @$(pwd)/temp/fake_docs.json
