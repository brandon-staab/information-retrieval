import os
import yaml

class Parameters:

    def __init__(self, dirpath, filepath):
        self.target_files = []
        self.target_dirs = []
        self.dirpath = dirpath
        self.filepath = filepath
        with open(dirpath + '/' + filepath, 'r') as stream:
            self.values = yaml.safe_load(stream)

    def add_target_file(self, filepath):
        self.target_files.append(filepath)

    def add_target_directory(self, dirpath):
        self.target_dirs.append(dirpath)

    def get_map(self):
        return self.flatten(self.values)

    def flatten(self, value, path="", paths=None):
        if paths is None:
            paths = {}
        if not isinstance(value, dict):
            paths[path] = value
        else:
            for key, child in value.items():
                new_path = key if len(path) == 0 else path + '.' + key
                paths = self.flatten(child, new_path, paths)
        return paths 

    def replace_in_file(self, filepath):
        variables = self.get_map()
        with open(filepath, 'r') as stream:
            contents = stream.read()
        for key, value in variables.items():
            placeholder = '%%' + key + '%%'
            contents = contents.replace(placeholder, str(value))
        with open(filepath, 'w') as stream:
            stream.write(contents)

    def replace_in_directory(self, dirpath):
        variables = self.get_map()
        for key, value in variables.items():
            for item in os.listdir(dirpath):
                item_path = dirpath + '/' + item
                if os.path.isfile(item_path):
                    self.replace_in_file(item_path)
                if os.path.isdir(item_path):
                    self.replace_in_directory(item_path)


    def replace_variables(self):
        for file in self.target_files:
            self.replace_in_file(self.dirpath + '/' + file)
        for directory in self.target_dirs:
            self.replace_in_directory(self.dirpath + '/' + directory)
