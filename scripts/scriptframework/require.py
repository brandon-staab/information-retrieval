import os

class Requirements:

    def __init__(self, config_root=""):
        self.config_root = config_root
        self.required_files = []
        self.required_directories = []

    def add_required_directory(self, dir_path):
        full_path = self.config_root + "/" + dir_path
        self.required_directories.append(full_path)

    def add_required_file(self, file_path):
        full_path = self.config_root + "/" + file_path
        self.required_files.append(full_path)

    def get_required_files(self):
        return self.required_files

    def get_required_directories(self):
        return self.required_directories

    def enforce(self):
        missing_directories = []
        for directory in self.required_directories:
            if not os.path.isdir(directory):
                missing_directories.append(directory)

        missing_files = []
        for file in self.required_files:
            if not os.path.isfile(file):
                missing_files.append(file)

        # enforce the "required directories" requirements
        if len(missing_directories) != 0:
            raise Exception("A required directory is missing.")

        # enforce the "required files" requirements
        if len(missing_files) != 0:
            raise Exception("A required file is missing.")

