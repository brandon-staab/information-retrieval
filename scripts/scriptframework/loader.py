import sys

class Loader:

    def __init__(self, module_path, module_name, class_name):
        self.module_path = module_path
        self.module_name = module_name
        self.class_name = class_name

    def get_class(self):
        sys.path.append(self.module_path)
        __import__(self.module_name, globals(), locals(), ['*'])
        return getattr(sys.modules[self.module_name], self.class_name)
