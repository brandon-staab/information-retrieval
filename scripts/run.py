import argparse
import os
import shutil
import sys

from scriptframework import loader

# add the utility classes to system path so that child directories
# can access shared code
sys.path.insert(0, 'scriptframework')

def main ():

    # parse command-line arguments
    parser = argparse.ArgumentParser(description='Deploy a runtime configuration')
    parser.add_argument('config_path')
    args = parser.parse_args()

    # copy selected configuration into a "temp" directory
    if os.path.isdir("temp"):
        shutil.rmtree("temp")
    shutil.copytree(args.config_path, "temp")

    # load selected runtime configuration
    class_loader = loader.Loader("temp", "runner", "Runner")
    Runner = class_loader.get_class()

    runner = Runner()
    runner.run()

if __name__ == '__main__':
    main()
