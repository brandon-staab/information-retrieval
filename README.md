# Term Project for 3460:489/589 Intro. to Info. Retrieval

Brandon Staab, Sean Collins

## About

We aim to provide efficient access to information that we scrape
from Linux Man pages. We want to shorten the amount of time it takes
command-line users to find the information they need.

## Prerequisites

This guide assumes that you are running `Ubuntu 19.10`.

First, install the following packages.

```bash
sudo apt-get install \
  docker             \
  docker-compose     \
  maven              \
  npm                \
  openjdk-14-jdk     \
  python3

pip3 install     \
  beautifulsoup4 \
  cachecontrol   \
  requests       \
```

Next, configure docker to run in unprivileged mode.

Then, initialize the `view` directory using: `npm install`.

## Running

Now, run each component in a separate shell.  For example:

```bash
# Shell #1 - Solr
cd ./scripts/
python3 run.py processes/solr/

# Shell #2 - Tomcat
cd ./scripts/
python3 run.py processes/tomcat/

# Shell #3 - View
cd ./view/
npm run serve

# Shell #4 - Scrape
cd ./scripts/
python3 run.py processes/scrape/
```
